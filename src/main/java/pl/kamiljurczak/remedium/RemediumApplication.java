package pl.kamiljurczak.remedium;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//klasa do uruchomienia aplikacji z osadzonym serwerem
@SpringBootApplication
public class RemediumApplication {

    public static void main(String[] args) {
        SpringApplication.run(RemediumApplication.class, args);
    }

}

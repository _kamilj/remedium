package pl.kamiljurczak.remedium.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pl.kamiljurczak.remedium.model.Patient;

@Controller
public class HomeController {

    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("patient", new Patient());
        return "addPatient";
    }
}

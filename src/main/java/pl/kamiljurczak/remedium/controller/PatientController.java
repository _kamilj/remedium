package pl.kamiljurczak.remedium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.kamiljurczak.remedium.data.PatientRepository;
import pl.kamiljurczak.remedium.model.Patient;

import java.util.List;

@Controller
public class PatientController {

    private PatientRepository patientRepository;

    @Autowired
    public void setPatientRepository(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    @PostMapping("/add")
    public String save(@ModelAttribute Patient patient) {
        patientRepository.save(patient);
        return "redirect:/";
    }

    @GetMapping("/patients")
    public String showAll(Model model) {
        List<Patient> patients = patientRepository.findAll();
        model.addAttribute("patients", patients);
        return "patients";
    }

    @GetMapping(value = "/patient", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    public Patient getPatient() {
        return new Patient("Kamil", "Jurczak", "90071900956");
    }
}

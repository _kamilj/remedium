package pl.kamiljurczak.remedium.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kamiljurczak.remedium.model.Patient;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {
}
